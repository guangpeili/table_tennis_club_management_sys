import { Component, OnInit } from '@angular/core';
import {Member} from '../../shared/models/member.model';
import {MemberService} from '../../shared/services/member.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-member-registration',
  templateUrl: './member-registration.component.html',
  styleUrls: ['./member-registration.component.scss']
})
export class MemberRegistrationComponent implements OnInit {

  member: Member = {
    memberId: 0,
    memberDetail: {
      id: 0,
      name: '',
      phone: '',
      email: '',
      address1: '',
      address2: '',
      city: '',
      state: '',
      zip: ''
    },
    memberSince: '',
    membershiptier: {
      tierId: 0,
      tierName: ''
    },
  };

  constructor(
    private ms: MemberService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  onSubmit() {
    this.ms.addMember(this.member).subscribe((res) => {
        if (res.success) {
          this.router.navigate(['/member']);
        }
    });
  }

}
