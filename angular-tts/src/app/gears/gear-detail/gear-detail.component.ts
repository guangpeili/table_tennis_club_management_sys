import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {GearService} from '../../shared/services/gear.service';
import {switchMap} from 'rxjs/operators';
import {Gear} from '../../shared/models/gear.model';

@Component({
  selector: 'app-gear-detail',
  templateUrl: './gear-detail.component.html',
  styleUrls: ['./gear-detail.component.scss']
})
export class GearDetailComponent implements OnInit {

  gearId: number;
  gear: Gear;

  action: number;
  amount: number;

  constructor(
    private ar: ActivatedRoute,
    private gs: GearService,
    private router: Router
  ) { }

  ngOnInit() {
    this.ar.paramMap
      .pipe(switchMap( params => {
        this.gearId = Number(params.get('gearId'));
        return this.gs.getGear(this.gearId);
      }))
      .subscribe((res: Gear) => {
        this.gear = res;
      });
  }

  testStock() {
    if (this.action === 1) {
      this.gear.amount += this.amount;
    } else {
      if ((this.gear.amount - this.amount) < 0) {
        confirm('Request exceeds current amount in stock');
      } else {
        this.gear.amount -= this.amount;
      }
    }
    this.gs.editGear(this.gear).subscribe(
      res => {
        if (res.success) {
          this.ngOnInit();
        }
      }
    );
  }

  back() {
    this.router.navigate(['/gear']);
  }

}
