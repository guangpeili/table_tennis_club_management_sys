import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-gear-overview',
  templateUrl: './gear-overview.component.html',
  styleUrls: ['./gear-overview.component.scss']
})
export class GearOverviewComponent implements OnInit {

  @Input() g;

  constructor() { }

  ngOnInit() {
    // console.log(this.g);
  }

}
