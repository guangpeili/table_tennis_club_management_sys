import { Component, OnInit } from '@angular/core';
import {Gear} from '../shared/models/gear.model';
import {GearService} from '../shared/services/gear.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-gears',
  templateUrl: './gears.component.html',
  styleUrls: ['./gears.component.scss']
})
export class GearsComponent implements OnInit {

  gears: Gear[] = [];
  getGear: Subscription;
  type = 'All';

  constructor(private gs: GearService) { }

  ngOnInit() {
    this.getGear = this.gs.getGears()
      .subscribe( {
        next: (res: Gear[]) => {
          this.gears = res;
        },
        complete: () => {
          console.log('getGears complete');
        }
      });
  }

}
