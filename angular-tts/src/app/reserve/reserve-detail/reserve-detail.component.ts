import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ReservationService} from '../../shared/services/reservation.service';
import {switchMap} from 'rxjs/operators';
import {Reservation} from '../../shared/models/reservation.model';
import {MemberService} from '../../shared/services/member.service';
import {Member} from '../../shared/models/member.model';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-reserve-detail',
  templateUrl: './reserve-detail.component.html',
  styleUrls: ['./reserve-detail.component.scss']
})
export class ReserveDetailComponent implements OnInit {

  reservationId: number;
  reservation: Reservation;

  member: Member;
  memberId: number;
  getMember: Subscription;

  constructor(
    private ar: ActivatedRoute,
    private rs: ReservationService,
    private ms: MemberService,
    private router: Router
  ) { }

  ngOnInit() {
    this.ar.paramMap
      .pipe(switchMap(params => {
        this.reservationId = Number(params.get('reservationId'));
        return this.rs.getReservationById(this.reservationId);
      }))
      .subscribe((res: Reservation) => {
        this.reservation = res;
        console.log(this.reservation);
        this.memberId = this.reservation.memberId;
        // console.log(this.memberId);

        this.getMember = this.ms.getMember(this.memberId)
          .subscribe((res1: Member) => {
            // console.log(res1);
            this.member = res1;
          });
      });
  }

  editReservation() {
    this.rs.editReservation(this.reservation).subscribe( res => {
      if (res.success) {
        this.router.navigate(['/reservation']);
      }
    });
  }
}
