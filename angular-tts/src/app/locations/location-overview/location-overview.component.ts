import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-location-overview',
  templateUrl: './location-overview.component.html',
  styleUrls: ['./location-overview.component.scss']
})
export class LocationOverviewComponent implements OnInit {

  @Input() l;

  constructor() { }

  ngOnInit() {
    // console.log(this.l);
  }

}
