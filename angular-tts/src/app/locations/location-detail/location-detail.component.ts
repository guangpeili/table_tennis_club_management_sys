import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {LocationService} from '../../shared/services/location.service';
import {switchMap} from 'rxjs/operators';
import {Location} from '../../shared/models/location.model';

@Component({
  selector: 'app-location-detail',
  templateUrl: './location-detail.component.html',
  styleUrls: ['./location-detail.component.scss']
})
export class LocationDetailComponent implements OnInit {

  locationId: number;
  location: Location;

  constructor(
    private ar: ActivatedRoute,
    private ls: LocationService
  ) {}

  ngOnInit() {
    this.ar.paramMap
      .pipe(switchMap( params => {
        this.locationId = Number(params.get('locationId'));
        return this.ls.getLocation(this.locationId);
        }))
      .subscribe((res: Location) => {
        this.location = res;
        console.log(this.location);
      });
  }
}
