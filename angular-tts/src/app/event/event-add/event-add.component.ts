import { Component, OnInit } from '@angular/core';
import {EventService} from '../../shared/services/event.service';
import {Event} from '../../shared/models/event.model';
import {Location} from '../../shared/models/location.model';
import {ActivatedRoute, Router} from '@angular/router';
import {switchMap} from 'rxjs/operators';
import {LocationService} from '../../shared/services/location.service';
import {Member} from '../../shared/models/member.model';
import {MemberService} from '../../shared/services/member.service';
import {Subscription} from 'rxjs';
import {Participant} from '../../shared/models/participant.model';
import {ParticipantService} from '../../shared/services/participant.service';

@Component({
  selector: 'app-event-add',
  templateUrl: './event-add.component.html',
  styleUrls: ['./event-add.component.scss']
})
export class EventAddComponent implements OnInit {

  location: Location;
  locationId: number;

  getMember: Subscription;
  newMember: Member;

  name: string;
  names: string[];

  addClicked = false;

  event: Event = {
    eventId: 0,
    eventType: {
      eventTypeId: 0,
      eventName: ''
    },
    location: this.location,
    date: '',
    timeStart: '',
    timeEnd: ''
  };

  constructor(
    private ar: ActivatedRoute,
    private router: Router,
    private es: EventService,
    private ls: LocationService,
    private ms: MemberService,
    private ps: ParticipantService
  ) { }

  ngOnInit() {
    this.ar.paramMap
      .pipe(switchMap( params => {
        this.locationId = Number(params.get('locationId'));
        return this.ls.getLocation(this.locationId);
      }))
      .subscribe((res: Location) => {
        this.location = res;
      });
  }

  // addParticipant() {
  //   this.addClicked = true;
  // }

  addEvent() {
    this.event.location = this.location;
    this.es.addEvent(this.event)
      .subscribe(res => {
        if (res.success) {
          this.router.navigate(['/event']);
        }
      });
  }

  // findMemberByName() {
  //   this.getMember = this.ms.getMemberByName(this.name)
  //     .subscribe( {
  //       next: (res: Member) => {
  //         this.newMember = res;
  //         console.log(this.newMember);
  //       },
  //       complete: () => {
  //         console.log('getByName complete');
  //       }
  //     });
  // }
  //
  // confirmAdd() {
  //   this.ps.addParticipant(new Participant(this.newMember.memberId, this.eventId))
  //     .subscribe(res => {
  //       if (res.success) {
  //         this.ngOnInit();
  //       }
  //     });
  // }

}
