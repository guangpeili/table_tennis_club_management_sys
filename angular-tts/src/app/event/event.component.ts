import { Component, OnInit } from '@angular/core';
import {Subscription} from 'rxjs';
import {EventService} from '../shared/services/event.service';
import {MatTableDataSource} from '@angular/material';
import {Event} from '../shared/models/event.model';
import {Router} from '@angular/router';

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.scss']
})
export class EventComponent implements OnInit {

  events: Event[];
  getEvents: Subscription;

  dataSource: MatTableDataSource<any> = new MatTableDataSource();
  displayedColumns = ['eventId', 'location', 'eventType', 'date', 'action'];

  constructor(
    private es: EventService,
    private router: Router
  ) { }

  ngOnInit() {
    this.getEvents = this.es.getEvents()
      .subscribe({
        next: (res: Event[]) => {
          this.events = res;
          this.dataSource.data = res;
          this.dataSource.filterPredicate = (data, filter) => {
            const dataStr: string = (data.location.name + data.eventType.eventName + data.timeStart + data.timeEnd + data.date)
              .toLowerCase();
            return (dataStr.indexOf(filter) !== -1);
          };
        }
      });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  delete(eventId: number) {
    if (confirm('Are you sure to cancel this reservation?')) {
      this.es.deleteEventById(eventId).subscribe( res => {
        if (res.success) {
          this.ngOnInit();
          this.router.navigate(['/event']);
        }
      });
    }
  }
}
