import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {EventService} from '../../shared/services/event.service';
import {switchMap} from 'rxjs/operators';
import {Event} from '../../shared/models/event.model';
import {Subscription} from 'rxjs';
import {Member} from '../../shared/models/member.model';
import {MemberService} from '../../shared/services/member.service';
import {ParticipantService} from '../../shared/services/participant.service';
import {Participant} from '../../shared/models/participant.model';

@Component({
  selector: 'app-event-detail',
  templateUrl: './event-detail.component.html',
  styleUrls: ['./event-detail.component.scss']
})
export class EventDetailComponent implements OnInit {

  eventId: number;
  event: Event;

  getMemberIds: Subscription;
  getMember: Subscription;
  memberIds: number[];
  members: Member[] = [];
  member: Member;
  newMember: Member;
  names: string[];
  name: string;

  addClicked = false;

  constructor(
    private router: Router,
    private ar: ActivatedRoute,
    private es: EventService,
    private ms: MemberService,
    private ps: ParticipantService
  ) { }

  ngOnInit() {
    this.ar.paramMap
      .pipe(switchMap(params => {
        this.eventId = Number(params.get('eventId'));
        return this.es.getEventById(this.eventId);
      }))
      .subscribe((res: Event) => {
        this.event = res;

        this.getMemberIds = this.es.getMemberIdsByEventId(this.eventId)
          .subscribe((res1: number[]) => {
            this.memberIds = res1;

            // tslint:disable-next-line:prefer-for-of
            for (let i = 0; i < this.memberIds.length; i++) {
              this.getMember = this.ms.getMember(this.memberIds[i])
                .subscribe((res2: Member) => {
                  this.member = res2;
                  this.members.push(this.member);
                  // console.log(this.members);

                  this.names = this.members.map(m => m.memberDetail.name);
                });
            }
          });
      });
  }

  editEvent() {
    this.es.editEvent(this.event)
      .subscribe(res => {
        if (res.success) {
          this.router.navigate(['/event']);
        }
      });
  }

  addParticipant() {
    this.addClicked = true;
  }

  findMemberByName() {
    this.getMember = this.ms.getMemberByName(this.name)
      .subscribe( {
        next: (res: Member) => {
          this.newMember = res;
          console.log(this.newMember);
        },
        complete: () => {
          console.log('getByName complete');
        }
      });
  }

  confirmAdd() {
    this.ps.addParticipant(new Participant(this.newMember.memberId, this.eventId))
      .subscribe(res => {
        if (res.success) {
          this.ngOnInit();
        }
      });
  }
}
