import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { LocationsComponent } from './locations/locations.component';
import { LocationDetailComponent } from './locations/location-detail/location-detail.component';
import { HomeComponent } from './home/home.component';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { LocationOverviewComponent } from './locations/location-overview/location-overview.component';
import { GearsComponent } from './gears/gears.component';
import { GearOverviewComponent } from './gears/gear-overview/gear-overview.component';
import { GearDetailComponent } from './gears/gear-detail/gear-detail.component';
import {GearCategoryPipe} from './shared/pipes/gear-category.pipe';
import {CustomStyleModule} from './shared/modules/custom-style/custom-style.module';
import { MemberComponent } from './member/member.component';
import { MemberDetailComponent } from './member/member-detail/member-detail.component';
import { MemberRegistrationComponent } from './member/member-registration/member-registration.component';
import { ReservationComponent } from './reservation/reservation.component';
import {MatDatepickerModule, MatPaginatorModule} from '@angular/material';
import { ReserveComponent } from './reserve/reserve.component';
import { ReserveDetailComponent } from './reserve/reserve-detail/reserve-detail.component';
import { EventComponent } from './event/event.component';
import { EventDetailComponent } from './event/event-detail/event-detail.component';
import { EventAddComponent } from './event/event-add/event-add.component';
import { LoginComponent } from './login/login.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    LocationsComponent,
    LocationDetailComponent,
    HomeComponent,
    LocationOverviewComponent,
    GearsComponent,
    GearOverviewComponent,
    GearDetailComponent,
    GearCategoryPipe,
    MemberComponent,
    MemberDetailComponent,
    MemberRegistrationComponent,
    ReservationComponent,
    ReserveComponent,
    ReserveDetailComponent,
    EventComponent,
    EventDetailComponent,
    EventAddComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    CustomStyleModule,
    MatPaginatorModule,
    MatDatepickerModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
