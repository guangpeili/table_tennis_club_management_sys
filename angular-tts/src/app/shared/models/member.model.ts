import {MemberDetail} from './memberDetail.model';
import {MembershipTier} from './membershipTier.model';

export class Member {

  public memberId?: number;
  public memberSince: string;
  public memberDetail: MemberDetail;
  public membershiptier: MembershipTier;

  constructor(memberId: number, memberSince: string, memberDetail: MemberDetail, membershiptier: MembershipTier) {
    this.memberId = memberId;
    this.memberSince = memberSince;
    this.memberDetail = memberDetail;
    this.membershiptier = membershiptier;
  }

}
