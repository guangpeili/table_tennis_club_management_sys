import {Location} from './location.model';

export class Reservation {
  public reservationId?: number;
  public memberId: number;
  public location: Location;
  public date: string;
  public startTime: string;
  public endTime: string;
  public robot: number;
}
