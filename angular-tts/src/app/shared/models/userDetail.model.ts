import {User} from './user.model';

export class UserDetail {
  public id?: number;
  public name: string;
  public phone: string;
  public email: string;
  public address1: string;
  public address2?: string;
  public city: string;
  public state: string;
  public zip: string;
  public user: User;
}
