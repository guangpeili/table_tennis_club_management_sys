import {EventType} from './eventType.model';
import {Location} from './location.model';

export class Event {
  public eventId: number;
  public eventType: EventType;
  public location: Location;
  public timeStart: string;
  public timeEnd: string;
  public date: string;
}
