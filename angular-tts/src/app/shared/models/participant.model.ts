export class Participant {
  public id?: number;
  public memberId: number;
  public eventId: number;

  constructor(memberId: number, eventId: number) {
    this.memberId = memberId;
    this.eventId = eventId;
  }
}
