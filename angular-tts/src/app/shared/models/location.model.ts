export class Location {

  public locationId?: number; // id is optional
  public name: string;
  public tel: string;
  public email: string;
  public address: string;
  public city: string;
  public zip: string;
  public hours: string;
}
