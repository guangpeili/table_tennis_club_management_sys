import {Profile} from './profile.model';
import {UserDetail} from './userDetail.model';

export class User {
  public id: number;
  public username: string;
  public password: string;
  public profiles: Profile[];
  public userDetail: UserDetail;
}

