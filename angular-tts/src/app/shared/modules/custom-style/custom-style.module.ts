import { NgModule } from '@angular/core';
import {
  MatButtonModule, MatToolbarModule, MatIconModule, MatInputModule,
  MatSidenavModule, MatListModule, MatCardModule, MatTableModule, MatDialogModule, MatSelectModule, MatBadgeModule,
  MatExpansionModule, MatRadioModule, MatSortModule, MatNativeDateModule
} from '@angular/material';
import {CdkTableModule} from '@angular/cdk/table';
import {FlexLayoutModule} from '@angular/flex-layout';

@NgModule({
  imports: [
    MatButtonModule,
    MatToolbarModule,
    MatIconModule,
    MatInputModule,
    MatSidenavModule,
    MatListModule,
    MatCardModule,
    MatTableModule,
    MatDialogModule,
    MatSelectModule,
    MatSortModule,
    MatBadgeModule,
    MatRadioModule,
    MatExpansionModule,
    MatNativeDateModule,
    CdkTableModule,
    FlexLayoutModule
  ],
  exports: [
    MatButtonModule,
    MatToolbarModule,
    MatIconModule,
    MatInputModule,
    MatSidenavModule,
    MatListModule,
    MatCardModule,
    MatTableModule,
    MatDialogModule,
    MatSelectModule,
    MatSortModule,
    MatBadgeModule,
    MatRadioModule,
    MatExpansionModule,
    MatNativeDateModule,
    CdkTableModule,
    FlexLayoutModule
  ]
})
export class CustomStyleModule { }
