import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs';
import {MembershipTier} from '../models/membershipTier.model';


@Injectable({
  providedIn: 'root'
})

export class TierService {

  constructor(private httpClient: HttpClient) {}

  public getNameById(id: number): Observable<MembershipTier> {
    return this.httpClient.get<MembershipTier>(`${environment.API_URL}/TtmUser/tier/${id}`);
  }

}
