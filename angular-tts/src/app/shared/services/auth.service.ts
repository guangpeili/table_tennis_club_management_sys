import {BehaviorSubject, Observable, Subject} from 'rxjs';
import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {User} from '../models/user.model';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class AuthService {

  user = null;
  userSubject: Subject<User> = new BehaviorSubject<User>(null);
  show;

  constructor(private httpClient: HttpClient) {
    // check localstorage for login info
    this.user = localStorage.getItem('user');
  }

  // content type because of spring security
  // json => form url encoded data
  login(user: User): Observable<{success: boolean, user: User}> {
    const httpParams: HttpParams = new HttpParams()
      .append('username', user.username)
      .append('password', user.password);
    return this.httpClient.post<{success: boolean, user: User}>(`http://localhost:8080/login`, httpParams, {withCredentials: true})
      .pipe(
        map((res: {success: boolean, user: User}) => {
          this.userSubject.next(res.user);
          return res;
        })
      );
  }

  logout() {
    this.show = false;
    return this.httpClient.post(`${environment.API_URL}/logout`, null, {withCredentials: true});
  }

}
