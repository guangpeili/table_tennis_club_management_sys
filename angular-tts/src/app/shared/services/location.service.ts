import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';
import {Location} from '../models/location.model';

@Injectable({
  providedIn: 'root'
})

export class LocationService {

  constructor(private httpClient: HttpClient) {}

  public getLocations(): Observable<Location[]> {
    return this.httpClient.get<Location[]>(`${environment.API_URL}/TtmEvent/location`);
  }

  public getLocation(id: number): Observable<Location> {
    return this.httpClient.get<Location>(`${environment.API_URL}/TtmEvent/location/${id}`);
  }

}
