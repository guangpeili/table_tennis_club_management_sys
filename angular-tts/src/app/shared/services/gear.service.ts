import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';
import {Gear} from '../models/gear.model';

@Injectable({
  providedIn: 'root'
})

export class GearService {

  constructor(private httpClient: HttpClient) {}

  public getGears(): Observable<Gear[]> {
    return this.httpClient.get<Gear[]>(`${environment.API_URL}/TtmOrder/gear`);
  }

  public getGear(id: number): Observable<Gear> {
    return this.httpClient.get<Gear>(`${environment.API_URL}/TtmOrder/gear/${id}`);
  }

  public editGear(gear: Gear): Observable<{success: boolean}> {
    return this.httpClient.put<{success: boolean}>(`${environment.API_URL}/TtmOrder/gear`, gear);
  }
}
