import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {Participant} from '../models/participant.model';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})

export class ParticipantService {

  constructor(private httpClient: HttpClient) {}

  public addParticipant(participant: Participant): Observable<{success: boolean}> {
    return this.httpClient.post<{success: boolean}>(`${environment.API_URL}/TtmEvent/participant`, participant);
  }

}
