import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';
import {Event} from '../models/event.model';
import {Member} from '../models/member.model';

@Injectable({
  providedIn: 'root'
})

export class EventService {

  constructor(private httpClient: HttpClient) {}

  public getEvents(): Observable<Event[]> {
    return this.httpClient.get<Event[]>(`${environment.API_URL}/TtmEvent/event`);
  }

  public getEventById(eventId: number): Observable<Event> {
    return this.httpClient.get<Event>(`${environment.API_URL}/TtmEvent/event/${eventId}`);
  }

  public getMemberIdsByEventId(eventId: number): Observable<number[]> {
    return this.httpClient.get<number[]>(`${environment.API_URL}/TtmEvent/participant/${eventId}`);
  }

  public editEvent(event: Event): Observable<{success: boolean}> {
    return this.httpClient.put<{success: boolean}>(`${environment.API_URL}/TtmEvent/event`, event);
  }

  public deleteEventById(eventId: number): Observable<{success: boolean}> {
    return this.httpClient.delete<{success: boolean}>(`${environment.API_URL}/TtmEvent/event/${eventId}`);
  }

  public addEvent(event: Event): Observable<{success: boolean}> {
    return this.httpClient.post<{success: boolean}>(`${environment.API_URL}/TtmEvent/event`, event);
  }
}
