import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Member} from '../models/member.model';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})

export class MemberService {

  members: Member[];

  constructor(private httpClient: HttpClient) {}

  public getMembers(): Observable<Member[]> {
    return this.httpClient.get<Member[]>(`${environment.API_URL}/TtmUser/member`);
  }

  public getMember(id: number): Observable<Member> {
    return this.httpClient.get<Member>(`${environment.API_URL}/TtmUser/member/${id}`);
  }

  public getMemberByName(name: string): Observable<Member> {
    return this.httpClient.get<Member>(`${environment.API_URL}/TtmUser/member/name/${name}`);
  }

  public editMember(member: Member): Observable<{success: boolean}> {
    return this.httpClient.put<{success: boolean}>(`${environment.API_URL}/TtmUser/member`, member);
  }

  public addMember(member: Member): Observable<{success: boolean}> {
    return this.httpClient.post<{success: boolean}>(`${environment.API_URL}/TtmUser/member`, member);
  }
}
