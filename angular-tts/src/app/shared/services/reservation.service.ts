import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';
import {Reservation} from '../models/reservation.model';

@Injectable ({
  providedIn: 'root'
})

export class ReservationService {

  constructor(private httpClient: HttpClient) {}

  public addReservation(reservation: Reservation): Observable<{success: boolean}> {
    return this.httpClient.post<{success: boolean}>(`${environment.API_URL}/TtmEvent/reservation`, reservation);
  }

  public editReservation(reservation: Reservation): Observable<{success: boolean}> {
    return this.httpClient.put<{success: boolean}>(`${environment.API_URL}/TtmEvent/reservation`, reservation);
  }

  public getReservations(): Observable<Reservation[]> {
    return this.httpClient.get<Reservation[]>(`${environment.API_URL}/TtmEvent/reservation`);
  }

  public getReservationById(id: number): Observable<Reservation> {
    return this.httpClient.get<Reservation>(`${environment.API_URL}/TtmEvent/reservation/${id}`);
  }

  public deleteReservationById(id: number): Observable<{success: boolean}> {
    return this.httpClient.delete<{success: boolean}>(`${environment.API_URL}/TtmEvent/reservation/${id}`);
  }
}
