package com.mercury.TtmOrder.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mercury.TtmOrder.beans.Gear;
import com.mercury.TtmOrder.http.Response;
import com.mercury.TtmOrder.services.GearService;

@RestController
@RequestMapping("/gear")
public class GearController {
	
	@Autowired
	private GearService gearService;
	
	@GetMapping("/{id}")
	public Gear getGearById(@PathVariable int id) {
		return gearService.getById(id);
	}
	
	@GetMapping("/name/{name}")
	public Gear getGearByName(@PathVariable String name) {
		return gearService.getGearByName(name);
	}
	
	@GetMapping
	public List<Gear> getAll() {
		return gearService.getAllGears();
	}
	
	@PutMapping
	public Response editGear(@RequestBody Gear gear) {
		return gearService.editGear(gear);
	}
	
}
