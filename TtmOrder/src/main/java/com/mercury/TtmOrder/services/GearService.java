package com.mercury.TtmOrder.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mercury.TtmOrder.beans.Gear;
import com.mercury.TtmOrder.daos.GearDao;
import com.mercury.TtmOrder.http.Response;

@Service
public class GearService {
	
	@Autowired
	private GearDao gearDao;
	
	public Gear getById(int id) {
		
		Optional<Gear> op = gearDao.findById(id);
		
		if (op.isPresent()) {
			return op.get();
		} else {
			return null;
		}
		
	}
	
	public Gear getGearByName(String name) {
		
		Optional<Gear> op = gearDao.findByName(name);
		
		if (op.isPresent()) {
			return op.get();
		} else {
			return null;
		}
		
	}
	
	public List<Gear> getAllGears() {
		return gearDao.findAll();
	}
	
	public Response editGear(Gear gear) {
		try {
			gearDao.save(gear);
			return new Response(true);
		} catch (Exception e) {
			return new Response(false, e.getMessage());
		}
	}
	
}
