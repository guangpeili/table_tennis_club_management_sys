package com.mercury.TtmOrder.daos;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mercury.TtmOrder.beans.OrderGear;

public interface OrderGearDao extends JpaRepository<OrderGear, Integer>{

}
