package com.mercury.TtmOrder.daos;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mercury.TtmOrder.beans.OrderBar;

public interface OrderBarDao extends JpaRepository<OrderBar, Integer>{

}
