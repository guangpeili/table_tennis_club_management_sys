package com.mercury.TtmOrder.daos;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mercury.TtmOrder.beans.Gear;

public interface GearDao extends JpaRepository<Gear, Integer>{
	
	Optional<Gear> findByName(String name);
	
	Optional<Gear> findById(int id);
}
