package com.mercury.TtmOrder.daos;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mercury.TtmOrder.beans.Bar;

public interface BarDao extends JpaRepository<Bar, Integer>{

}
