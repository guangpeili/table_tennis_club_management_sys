package com.mercury.TtmOrder.daos;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mercury.TtmOrder.beans.OrderForGear;

public interface OrderForGearDao extends JpaRepository<OrderForGear, Integer>{

}
