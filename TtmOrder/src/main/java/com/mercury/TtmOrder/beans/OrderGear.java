package com.mercury.TtmOrder.beans;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="ORDER_GEAR")
public class OrderGear {
	
	@Id
	@SequenceGenerator(name="order_gear_seq_gen", sequenceName="ORDER_GEAR_SEQ", allocationSize=1)
	@GeneratedValue(generator="order_gear_seq_gen", strategy=GenerationType.AUTO)
	private int id;
	
	@Column
	private int quantity;
	
	@ManyToOne(fetch=FetchType.EAGER, cascade=CascadeType.DETACH)
	@JoinColumn(name="ORDER_ID")
	private OrderForGear orderForGear;
	
	@ManyToOne(fetch=FetchType.EAGER, cascade=CascadeType.DETACH)
	@JoinColumn(name="GEAR_ID")
	private Gear gear;

	public OrderGear() {
		super();
	}

	public OrderGear(int id, int quantity, OrderForGear orderForGear, Gear gear) {
		super();
		this.id = id;
		this.quantity = quantity;
		this.orderForGear = orderForGear;
		this.gear = gear;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	@JsonIgnore
	public OrderForGear getOrderForGear() {
		return orderForGear;
	}

	public void setOrderForGear(OrderForGear orderForGear) {
		this.orderForGear = orderForGear;
	}

	public Gear getGear() {
		return gear;
	}

	public void setGear(Gear gear) {
		this.gear = gear;
	}

	@Override
	public String toString() {
		return "OrderGear [id=" + id + ", quantity=" + quantity + ", orderForGear=" + orderForGear + ", gear=" + gear
				+ "]";
	}
	
}
