package com.mercury.TtmOrder.beans;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="ORDER_FROM_BAR")
public class OrderFromBar {
	
	@Id
	@SequenceGenerator(name = "order_from_bar_seq_gen", sequenceName = "ORDER_FROM_BAR_SEQ", allocationSize = 1)
	@GeneratedValue(generator="order_from_bar_seq_gen", strategy = GenerationType.AUTO)
	private int orderId;
	
	@Column(name="PURCHASE_DATE")
	private String purchaseDate;
	
	@Column(name="MEMBER_ID")
	private int memberId;
	
	@Column(name="LOCATION_ID")
	private int locationId;
	
	@OneToMany(fetch=FetchType.EAGER, cascade=CascadeType.ALL, mappedBy="orderFromBar")
	private List<OrderBar> purchases;

	public OrderFromBar() {
		super();
	}

	public OrderFromBar(int orderId, String purchaseDate, int memberId, int locationId, List<OrderBar> purchases) {
		super();
		this.orderId = orderId;
		this.purchaseDate = purchaseDate;
		this.memberId = memberId;
		this.locationId = locationId;
		this.purchases = purchases;
	}

	public int getOrderId() {
		return orderId;
	}

	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}

	public String getPurchaseDate() {
		return purchaseDate;
	}

	public void setPurchaseDate(String purchaseDate) {
		this.purchaseDate = purchaseDate;
	}

	public int getMemberId() {
		return memberId;
	}

	public void setMemberId(int memberId) {
		this.memberId = memberId;
	}

	public int getLocationId() {
		return locationId;
	}

	public void setLocationId(int locationId) {
		this.locationId = locationId;
	}

	public List<OrderBar> getPurchases() {
		return purchases;
	}

	public void setPurchases(List<OrderBar> purchases) {
		this.purchases = purchases;
	}

	@Override
	public String toString() {
		return "OrderFromBar [orderId=" + orderId + ", purchaseDate=" + purchaseDate + ", memberId=" + memberId
				+ ", locationId=" + locationId + ", purchases=" + purchases + "]";
	}

}
