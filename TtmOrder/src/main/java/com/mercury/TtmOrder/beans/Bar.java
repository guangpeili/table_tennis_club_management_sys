package com.mercury.TtmOrder.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="BAR")
public class Bar {
	
	@Id
	@SequenceGenerator(name="bar_seq_gen", sequenceName="BAR_SEQ", allocationSize=1)
	@GeneratedValue(generator="bar_seq_gen", strategy=GenerationType.AUTO)
	private int productId;
	
	@Column
	private String type;
	
	@Column(name="CONTAINS_ALCOHOL")
	private int containsAlcohol;
	
	@Column
	private String name;
	
	@Column
	private String brand;
	
	@Column
	private int price;
	
	@Column
	private String image;

	public Bar() {
		super();
	}

	public Bar(int productId, String type, int containsAlcohol, String name, String brand, int price, String image) {
		super();
		this.productId = productId;
		this.type = type;
		this.containsAlcohol = containsAlcohol;
		this.name = name;
		this.brand = brand;
		this.price = price;
		this.image = image;
	}

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getContainsAlcohol() {
		return containsAlcohol;
	}

	public void setContainsAlcohol(int containsAlcohol) {
		this.containsAlcohol = containsAlcohol;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	@Override
	public String toString() {
		return "Bar [productId=" + productId + ", type=" + type + ", containsAlcohol=" + containsAlcohol + ", name="
				+ name + ", brand=" + brand + ", price=" + price + ", image=" + image + "]";
	}

}
