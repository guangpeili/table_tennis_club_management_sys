package com.mercury.TtmUser.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mercury.TtmUser.beans.Member;
import com.mercury.TtmUser.http.Response;
import com.mercury.TtmUser.services.MemberService;

@RestController
@RequestMapping("/member")
public class MemberController {
	
	@Autowired
	private MemberService memberService;
	
	@GetMapping("/{id}")
	public Member getMemberById(@PathVariable int id) {
		return memberService.getById(id);
	}
	
	@GetMapping("/name/{name}")
	public Member getMemberByName(@PathVariable String name) {
		return memberService.getByName(name);
	}
	
	@GetMapping
	public List<Member> getAll() {
		return memberService.getAllMembers();
	}
	
	@GetMapping("/list")
	public List<Member> getByIds(@RequestBody List<Integer> ids) {
		return memberService.getByIds(ids);
	}
	
	@PutMapping
	public Response update(@RequestBody Member member) {
		return memberService.edit(member);
	}
	
	@PostMapping
	public Response register(@RequestBody Member member) {
		return memberService.create(member);
	}
	
}
