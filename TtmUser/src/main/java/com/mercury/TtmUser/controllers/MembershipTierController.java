package com.mercury.TtmUser.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mercury.TtmUser.beans.MembershipTier;
import com.mercury.TtmUser.services.MembershipTierService;

@RestController
@RequestMapping("/tier")
public class MembershipTierController {

	@Autowired
	private MembershipTierService membershipTierService;
	
	@GetMapping("/{id}")
	public MembershipTier getNameById(@PathVariable int id) {
		return membershipTierService.getTierNameById(id);
	}
	
}
