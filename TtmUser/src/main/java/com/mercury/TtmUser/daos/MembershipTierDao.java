package com.mercury.TtmUser.daos;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.mercury.TtmUser.beans.MembershipTier;

public interface MembershipTierDao extends JpaRepository<MembershipTier, Integer>{
	
	@Query("select t from MembershipTier t where t.tierId = :id")
	Optional<MembershipTier> getNameById(@Param("id") int id);
	
}
