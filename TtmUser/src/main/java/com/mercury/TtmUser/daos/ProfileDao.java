package com.mercury.TtmUser.daos;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mercury.TtmUser.beans.Profile;

public interface ProfileDao extends JpaRepository<Profile, Integer>{

}
