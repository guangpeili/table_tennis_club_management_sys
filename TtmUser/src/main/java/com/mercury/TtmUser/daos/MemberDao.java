package com.mercury.TtmUser.daos;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.mercury.TtmUser.beans.Member;

public interface MemberDao extends JpaRepository<Member, Integer >{
	
	Optional<Member> findById(int id);
	
	List<Member> findAll();
	
	@Query("select m from Member m where m.memberDetail.name = :name")
	Optional<Member> getByName(@Param("name") String name);
	
}
