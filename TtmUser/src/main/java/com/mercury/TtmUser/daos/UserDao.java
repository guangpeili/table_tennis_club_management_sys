package com.mercury.TtmUser.daos;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mercury.TtmUser.beans.User;

public interface UserDao extends JpaRepository<User, Integer>{

}
