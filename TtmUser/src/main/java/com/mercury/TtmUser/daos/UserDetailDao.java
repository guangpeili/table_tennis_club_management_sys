package com.mercury.TtmUser.daos;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mercury.TtmUser.beans.UserDetail;

public interface UserDetailDao extends JpaRepository<UserDetail, Integer> {

}
