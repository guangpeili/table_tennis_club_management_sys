package com.mercury.TtmUser;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class TtmUserApplication {

	public static void main(String[] args) {
		SpringApplication.run(TtmUserApplication.class, args);
	}

}
