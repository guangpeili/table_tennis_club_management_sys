package com.mercury.TtmUser.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="MEMBERSHIP_TIERS")
public class MembershipTier {
	
	@Id
	@SequenceGenerator(name="membership_tiers_seq_gen", sequenceName="MEMBERSHIP_TIERS_SEQ", allocationSize=1)
	@GeneratedValue(generator="membership_tiers_seq_gen", strategy=GenerationType.AUTO)
	private int tierId;
	
	@Column(name="TIER_NAME")
	private String tierName;

	public MembershipTier() {
		super();
	}

	public MembershipTier(int tierId, String tierName) {
		super();
		this.tierId = tierId;
		this.tierName = tierName;
	}

	public int getTierId() {
		return tierId;
	}

	public void setTierId(int tierId) {
		this.tierId = tierId;
	}

	public String getTierName() {
		return tierName;
	}

	public void setTierName(String tierName) {
		this.tierName = tierName;
	}

	@Override
	public String toString() {
		return "MembershipTier [tierId=" + tierId + ", tierName=" + tierName + "]";
	}
	
}
