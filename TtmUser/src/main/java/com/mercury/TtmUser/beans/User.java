package com.mercury.TtmUser.beans;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="TTM_USER")
public class User {
	
	@Id
	@SequenceGenerator(name="user_seq_gen", sequenceName="USER_SEQ", allocationSize=1)
	@GeneratedValue(generator="user_seq_gen", strategy=GenerationType.AUTO)
	private int id;
	
	@Column
	private String username;
	
	@Column
	private String password;
	
	@ManyToMany(fetch=FetchType.EAGER, cascade=CascadeType.DETACH)
	@JoinTable(
		name="USER_USER_PROFILE",
		joinColumns= {
			@JoinColumn(name="USER_ID", referencedColumnName="ID")	
		},
		inverseJoinColumns = {
			@JoinColumn(name="USER_PROFILE_ID", referencedColumnName="ID")
		}
	)
	private List<Profile> profiles;
	
	@OneToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL, mappedBy="user")
	private UserDetail userDetail;

	public User() {
		super();
		// TODO Auto-generated constructor stub
	}

	public User(int id, String username, String password, List<Profile> profiles, UserDetail userDetail) {
		super();
		this.id = id;
		this.username = username;
		this.password = password;
		this.profiles = profiles;
		this.userDetail = userDetail;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<Profile> getProfiles() {
		return profiles;
	}

	public void setProfiles(List<Profile> profiles) {
		this.profiles = profiles;
	}

	public UserDetail getUserDetail() {
		return userDetail;
	}

	public void setUserDetail(UserDetail userDetail) {
		this.userDetail = userDetail;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", username=" + username + ", password=" + password + ", profiles=" + profiles
				+ ", userDetail=" + userDetail + "]";
	}
	
}
