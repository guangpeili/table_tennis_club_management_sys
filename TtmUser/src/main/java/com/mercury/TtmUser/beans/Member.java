package com.mercury.TtmUser.beans;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="MEMBERS")
public class Member {
	
	@Id
	@SequenceGenerator(name="members_seq_gen", sequenceName="MEMBERS_SEQ", allocationSize=1)
	@GeneratedValue(generator="members_seq_gen", strategy=GenerationType.AUTO)
	private int memberId;
	
	@Column(name="MEMBER_SINCE")
	private String memberSince;
	
	@OneToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL, mappedBy="member")
	private MemberDetail memberDetail;
	
	@ManyToOne(fetch=FetchType.EAGER, cascade=CascadeType.DETACH)
	@JoinColumn(name="TIER")
	private MembershipTier membershiptier;

	public Member() {
		super();
	}

	public Member(int memberId, String memberSince, MemberDetail memberDetail, MembershipTier membershiptier) {
		super();
		this.memberId = memberId;
		this.memberSince = memberSince;
		this.memberDetail = memberDetail;
		this.membershiptier = membershiptier;
	}

	public int getMemberId() {
		return memberId;
	}

	public void setMemberId(int memberId) {
		this.memberId = memberId;
	}

	public String getMemberSince() {
		return memberSince;
	}

	public void setMemberSince(String memberSince) {
		this.memberSince = memberSince;
	}

	public MemberDetail getMemberDetail() {
		return memberDetail;
	}

	public void setMemberDetail(MemberDetail memberDetail) {
		this.memberDetail = memberDetail;
	}

	public MembershipTier getMembershiptier() {
		return membershiptier;
	}

	public void setMembershiptier(MembershipTier membershiptier) {
		this.membershiptier = membershiptier;
	}

	@Override
	public String toString() {
		return "Member [memberId=" + memberId + ", memberSince=" + memberSince + ", memberDetail=" + memberDetail
				+ ", membershiptier=" + membershiptier + "]";
	}
	
}
