package com.mercury.TtmUser.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mercury.TtmUser.beans.Member;
import com.mercury.TtmUser.daos.MemberDao;
import com.mercury.TtmUser.daos.MembershipTierDao;
import com.mercury.TtmUser.http.Response;

@Service
public class MemberService {
	
	@Autowired
	private MemberDao memberDao;
	
	@Autowired
	private MembershipTierDao membershipTierDao;
	
	public List<Member> getAllMembers() {
		return memberDao.findAll();
	}
	
	public Member getById(int id) {
		
		Optional<Member> om = memberDao.findById(id);
		
		if (om.isPresent()) {
			return om.get();
		} else {
			return null;
		}
	}
	
	public List<Member> getByIds(List<Integer> ids) {
		List<Member> res = new ArrayList<>();
		
		ids.forEach(i -> {
			Member m = memberDao.findById(i).get();
			res.add(m);
		});
		
		return res;
	}
	
	public Member getByName(String name) {
		Optional<Member> om = memberDao.getByName(name);
		
		if (om.isPresent()) {
			return om.get();
		} else {
			return null;
		}
	}
	
	public Response edit(Member member) {
		try {
			Member m = memberDao.findById(member.getMemberId()).get();
			m.setMemberDetail(member.getMemberDetail());
			m.getMemberDetail().setMember(m);
			m.setMembershiptier(membershipTierDao.findById(member.getMembershiptier().getTierId()).get());;
			memberDao.save(m);
			return new Response(true);
		} catch (Exception e) {
			return new Response(false, e.getMessage());
		}
	}
	
	public Response create(Member member) {
		try {
			
			member.getMemberDetail().setMember(member);
			
			memberDao.save(member);
			return new Response(true);
		} catch (Exception e) {
			return new Response(false, e.getMessage());
		}
	}
}






