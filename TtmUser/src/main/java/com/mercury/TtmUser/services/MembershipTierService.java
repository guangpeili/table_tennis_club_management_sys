package com.mercury.TtmUser.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mercury.TtmUser.beans.MembershipTier;
import com.mercury.TtmUser.daos.MembershipTierDao;

@Service
public class MembershipTierService {
	
	@Autowired
	private MembershipTierDao membershipTierDao;
	
	public MembershipTier getTierNameById(int id) {
		Optional<MembershipTier> name = membershipTierDao.getNameById(id);
		
		if (name.isPresent()) {
			return name.get();
		} else {
			return null;
		}
	}
	
}
