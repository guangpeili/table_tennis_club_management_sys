package com.mercury.TtmEvent.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mercury.TtmEvent.beans.Reservation;
import com.mercury.TtmEvent.http.Response;
import com.mercury.TtmEvent.services.ReservationService;

@RestController
@RequestMapping("/reservation")
public class ReservationController {
	
	@Autowired
	private ReservationService reservationService;
	
	@GetMapping
	public List<Reservation> getAll() {
		return reservationService.getAll();
	}
	
	@GetMapping("/{id}")
	public Reservation getById(@PathVariable int id) {
		return reservationService.getById(id);
	}
	
	@PutMapping()
	public Response editReservation(@RequestBody Reservation reservation) {
		return reservationService.editReservation(reservation);
	}
	
	@PostMapping
	public Response createReservation(@RequestBody Reservation reservation) {
		return reservationService.makeReservation(reservation);
	}
	
	@DeleteMapping("/{reservationId}")
	public Response deleteReservation(@PathVariable int reservationId) {
		return reservationService.deleteReservationById(reservationId);
	}
}
