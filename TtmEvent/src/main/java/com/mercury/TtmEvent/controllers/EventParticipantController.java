package com.mercury.TtmEvent.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mercury.TtmEvent.beans.EventParticipant;
import com.mercury.TtmEvent.http.Response;
import com.mercury.TtmEvent.services.EventParticipantService;

@RestController
@RequestMapping("/participant")
public class EventParticipantController {
	
	@Autowired
	private EventParticipantService eventParticipantService;
	
	@GetMapping("/{eventId}")
	public List<Integer> getMemberIdByEventId(@PathVariable int eventId) {
		return eventParticipantService.getMemberIdByEvent(eventId);
	}
	
	@PostMapping
	public Response addParticipant(@RequestBody EventParticipant eventParticipant) {
		return eventParticipantService.addParticipant(eventParticipant);
	}
	
}
