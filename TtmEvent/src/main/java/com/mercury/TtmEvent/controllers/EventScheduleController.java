package com.mercury.TtmEvent.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mercury.TtmEvent.beans.EventSchedule;
import com.mercury.TtmEvent.http.Response;
import com.mercury.TtmEvent.services.EventScheduleService;

@RestController
@RequestMapping("/event")
public class EventScheduleController {
	
	@Autowired
	private EventScheduleService eventScheduleService;
	
	@GetMapping
	public List<EventSchedule> getAllEvents() {
		return eventScheduleService.getAllEvents();
	}
	
	@GetMapping("/{eventId}")
	public EventSchedule getEventById(@PathVariable int eventId) {
		return eventScheduleService.getEventById(eventId);
	}
	
	@PutMapping
	public Response editSchedule(@RequestBody EventSchedule eventSchedule) {
		return eventScheduleService.editEvent(eventSchedule);
	}
	
	@DeleteMapping("/{eventId}")
	public Response deleteScheduleById(@PathVariable int eventId) {
		return eventScheduleService.deleteEventById(eventId);
	}
	
	@PostMapping
	public Response addEventSchedule(@RequestBody EventSchedule eventSchedule) {
		return eventScheduleService.addEvent(eventSchedule);
	}
	
}









