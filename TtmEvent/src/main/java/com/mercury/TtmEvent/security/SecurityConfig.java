//package com.mercury.TtmEvent.security;
//
//import java.util.Arrays;
//
//import org.springframework.context.annotation.Bean;
//import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
//import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
//import org.springframework.web.cors.CorsConfiguration;
//import org.springframework.web.cors.CorsConfigurationSource;
//import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
//
//@EnableGlobalMethodSecurity(prePostEnabled = true)
//@EnableWebSecurity
//public class SecurityConfig extends WebSecurityConfigurerAdapter {
//	
//	@Override
//	public void configure(HttpSecurity http) throws Exception {
//		
//		http
//			.cors()
//				.and()
//			.csrf()
//				.disable()
//			.authorizeRequests()
//				.anyRequest().permitAll();
//		
//	}
//	
//	 @Bean
//	 public CorsConfigurationSource corsConfigurationSource() {
//		 CorsConfiguration configuration = new CorsConfiguration();
//	     configuration.addAllowedOrigin("*"); // You should only set trusted site here. e.g. http://localhost:4200 means only this site can access.
//	     configuration.setAllowedMethods(Arrays.asList("GET","POST","PUT","DELETE","HEAD","OPTIONS"));
//	     configuration.addAllowedHeader("*");
//	     configuration.setAllowCredentials(true);
//	     UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
//	     source.registerCorsConfiguration("/**", configuration);
//	     return source;
//	 }
//	
//}
