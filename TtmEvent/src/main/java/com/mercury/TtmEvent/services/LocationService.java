package com.mercury.TtmEvent.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mercury.TtmEvent.beans.Location;
import com.mercury.TtmEvent.daos.LocationDao;

@Service
public class LocationService {
	
	@Autowired
	private LocationDao locationDao;
	
	public List<Location> getLocations() {
		
		List<Location> res = locationDao.findAll();
		
		return res;
	}
	
	public Location getById(int id) {
		
		Optional<Location> ol = locationDao.findByLocationId(id);
		
		if (ol.isPresent()) {
			return ol.get();
		} else {
			return null;
		}
	}
	
}
