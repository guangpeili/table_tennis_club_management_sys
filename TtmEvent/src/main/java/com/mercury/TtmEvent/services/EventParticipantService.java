package com.mercury.TtmEvent.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mercury.TtmEvent.beans.EventParticipant;
import com.mercury.TtmEvent.daos.EventParticipantDao;
import com.mercury.TtmEvent.http.Response;

@Service
public class EventParticipantService {
	
	@Autowired
	private EventParticipantDao eventParticipantDao;
	
	public List<Integer> getMemberIdByEvent(int eventId) {
		return this.eventParticipantDao.getMemberIdByEventId(eventId);
	}
	
	public Response addParticipant(EventParticipant eventParticipant) {
		try {
			eventParticipantDao.save(eventParticipant);
			return new Response(true);
		} catch (Exception e) {
			return new Response(false, e.getMessage());
		}
	}
}
