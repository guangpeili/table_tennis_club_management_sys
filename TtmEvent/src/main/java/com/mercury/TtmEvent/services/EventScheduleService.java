package com.mercury.TtmEvent.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mercury.TtmEvent.beans.EventSchedule;
import com.mercury.TtmEvent.daos.EventScheduleDao;
import com.mercury.TtmEvent.http.Response;

@Service
public class EventScheduleService {
	
	@Autowired
	private EventScheduleDao eventScheduleDao;
	
	public List<EventSchedule> getAllEvents() {
		return eventScheduleDao.findAll();
	}
	
	public EventSchedule getEventById(int eventId) {
		Optional<EventSchedule> oe = eventScheduleDao.findById(eventId);
		
		if (oe.isPresent()) {
			return oe.get();
		} else {
			return null;
		}
	}
	
	public Response editEvent(EventSchedule event) {
		try {
			eventScheduleDao.save(event);
			return new Response(true);
		} catch (Exception e) {
			return new Response(false, e.getMessage());
		}
	}
	
	public Response deleteEventById(int eventId) {
		try {
			eventScheduleDao.deleteById(eventId);
			return new Response(true);
		} catch (Exception e) {
			return new Response(false, e.getMessage());
		}
	}
	
	public Response addEvent(EventSchedule eventSchedule) {
		try {
			eventScheduleDao.save(eventSchedule);
			return new Response(true);
		} catch (Exception e) {
			return new Response(false, e.getMessage());
		}
	}
}










