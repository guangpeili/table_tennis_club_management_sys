package com.mercury.TtmEvent.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="EVENT_TYPES")
public class EventType {
	
	@Id
	@SequenceGenerator(name="event_types_seq_gen", sequenceName="EVENT_TYPES_SEQ", allocationSize = 1)
	@GeneratedValue(generator="event_types_seq_gen", strategy=GenerationType.AUTO)
	private int eventTypeId;
	
	@Column(name="EVENT_NAME")
	private String eventName;

	public EventType() {
		super();
	}

	public EventType(int eventTypeId, String eventName) {
		super();
		this.eventTypeId = eventTypeId;
		this.eventName = eventName;
	}

	public int getEventTypeId() {
		return eventTypeId;
	}

	public void setEventTypeId(int eventTypeId) {
		this.eventTypeId = eventTypeId;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	@Override
	public String toString() {
		return "EventType [eventTypeId=" + eventTypeId + ", eventName=" + eventName + "]";
	}
	
}
