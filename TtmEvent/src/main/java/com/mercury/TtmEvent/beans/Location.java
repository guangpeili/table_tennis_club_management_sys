package com.mercury.TtmEvent.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="LOCATIONS")
public class Location {
	
	@Id
	@SequenceGenerator(name = "locations_seq_gen", sequenceName = "LOCATIONS_SEQ", allocationSize = 1)
	@GeneratedValue(generator="locations_seq_gen", strategy = GenerationType.AUTO)
	private int locationId;
	
	@Column
	private String name;
	
	@Column
	private String tel;
	
	@Column
	private String email;
	
	@Column
	private String address;
	
	@Column
	private String city;
	
	@Column
	private String state;
	
	@Column
	private String hours;
	
	@Column
	private String image;

	public Location() {
		super();
	}

	public Location(int locationId, String name, String tel, String email, String address, String city, String state,
			String hours, String image) {
		super();
		this.locationId = locationId;
		this.name = name;
		this.tel = tel;
		this.email = email;
		this.address = address;
		this.city = city;
		this.state = state;
		this.hours = hours;
		this.image = image;
	}

	public int getLocationId() {
		return locationId;
	}

	public void setLocationId(int locationId) {
		this.locationId = locationId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getHours() {
		return hours;
	}

	public void setHours(String hours) {
		this.hours = hours;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	@Override
	public String toString() {
		return "Location [locationId=" + locationId + ", name=" + name + ", tel=" + tel + ", email=" + email
				+ ", address=" + address + ", city=" + city + ", state=" + state + ", hours=" + hours + ", image="
				+ image + "]";
	}
	
}
