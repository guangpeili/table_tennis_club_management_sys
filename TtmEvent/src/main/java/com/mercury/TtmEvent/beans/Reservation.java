package com.mercury.TtmEvent.beans;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="RESERVATIONS")
public class Reservation {
	
	@Id
	@SequenceGenerator(name = "reservations_seq_gen", sequenceName = "RESERVATIONS_SEQ", allocationSize = 1)
	@GeneratedValue(generator="reservations_seq_gen", strategy = GenerationType.AUTO)
	private int reservationId;
	
	@Column(name="MEMBER_ID")
	private int memberId;
	
	@ManyToOne(fetch=FetchType.EAGER, cascade=CascadeType.DETACH)
	@JoinColumn(name="LOCATION_ID")
	private Location location;
	
	@Column(name="R_DATE")
	private String date;
	
	@Column(name="START_TIME")
	private String startTime;
	
	@Column(name="END_TIME")
	private String endTime;
	
	@Column(name="ROBOT_NEEDED")
	private int robot;

	public Reservation() {
		super();
	}

	public Reservation(int reservationId, int memberId, Location location, String date, String startTime,
			String endTime, int robot) {
		super();
		this.reservationId = reservationId;
		this.memberId = memberId;
		this.location = location;
		this.date = date;
		this.startTime = startTime;
		this.endTime = endTime;
		this.robot = robot;
	}

	public int getReservationId() {
		return reservationId;
	}

	public void setReservationId(int reservationId) {
		this.reservationId = reservationId;
	}

	public int getMemberId() {
		return memberId;
	}

	public void setMemberId(int memberId) {
		this.memberId = memberId;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public int getRobot() {
		return robot;
	}

	public void setRobot(int robot) {
		this.robot = robot;
	}

	@Override
	public String toString() {
		return "Reservation [reservationId=" + reservationId + ", memberId=" + memberId + ", location=" + location
				+ ", date=" + date + ", startTime=" + startTime + ", endTime=" + endTime + ", robot=" + robot + "]";
	}
	
}
