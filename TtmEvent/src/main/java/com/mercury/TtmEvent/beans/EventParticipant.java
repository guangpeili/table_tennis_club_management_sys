package com.mercury.TtmEvent.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

// need to manually add participants separately since have no access to join table 
@Entity
@Table(name="EVENT_PARTICIPANTS")
public class EventParticipant {
	
	@Id
	@SequenceGenerator(name = "event_participants_seq_gen", sequenceName = "EVENT_PARTICIPANTS_SEQ", allocationSize = 1)
	@GeneratedValue(generator="event_participants_seq_gen", strategy = GenerationType.AUTO)
	private int id;
	
	@Column(name="MEMBER_ID")
	private int memberId;
	
	@Column(name="EVENT_ID")
	private int eventId;

	public EventParticipant() {
		super();
	}

	public EventParticipant(int id, int memberId, int eventId) {
		super();
		this.id = id;
		this.memberId = memberId;
		this.eventId = eventId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getMemberId() {
		return memberId;
	}

	public void setMemberId(int memberId) {
		this.memberId = memberId;
	}

	public int getEventId() {
		return eventId;
	}

	public void setEventId(int eventId) {
		this.eventId = eventId;
	}

	@Override
	public String toString() {
		return "EventParticipant [id=" + id + ", memberId=" + memberId + ", eventId=" + eventId + "]";
	}
	
}
