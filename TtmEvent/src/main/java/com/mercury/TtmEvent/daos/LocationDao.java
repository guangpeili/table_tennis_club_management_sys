package com.mercury.TtmEvent.daos;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mercury.TtmEvent.beans.Location;

public interface LocationDao extends JpaRepository<Location, Integer>{
	
	Optional<Location> findByLocationId(int id);
	
	List<Location> findAll();
	
}
