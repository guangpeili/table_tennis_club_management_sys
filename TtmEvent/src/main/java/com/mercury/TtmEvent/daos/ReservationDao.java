package com.mercury.TtmEvent.daos;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mercury.TtmEvent.beans.Reservation;

public interface ReservationDao extends JpaRepository<Reservation, Integer>{
	
	Optional<Reservation> findById(int id);
	
	List<Reservation> findAll();
	
}
