package com.mercury.TtmEvent.daos;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mercury.TtmEvent.beans.EventType;

public interface EventTypeDao extends JpaRepository<EventType, Integer>{

}
